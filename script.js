/*Asynchronous JavaScript and XML
Отримання данних з сервера у фоновому режимі і використання цих данних для оновлення сторінки(без перезавантаження).
Дії виконуються асинхронно, JavaScript мова на якій створюється та налаштовується запит, XML або JSON(також є інщі) формати в 
яких зберігаються і передаються данні. При відправленні такого запиту зі сторінкою можна далі взаємодіяти і не чекати відповіді.
Объект XMLHttpRequest(XHR) та fetch(також є інщі методи) дозволяють взаємодіяти з сервером через AJAX.*/

const wrapper = document.querySelector('.wrapper');

fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then(films => {
        console.log(films)
        films.forEach(film => {
            const wrapperForFilm = document.createElement('li');
            wrapperForFilm.innerHTML =
                `<h2>${film.name}</h2>
    <div class="actors-${film.episodeId}">Актори:</div>
    <p>Епізод: ${film.episodeId}</p>
    <p>Короткий зміст: ${film.openingCrawl}</p>`;
            wrapper.append(wrapperForFilm);

            film.characters.forEach(actor => {
                fetch(actor).then(person => person.json()).then(person => {
                    console.log(person);
                    const wrapperForPersons = document.querySelector(`.actors-${film.episodeId}`);
                    const actorInfo = document.createElement('p');
                    for (key in person) {
                        actorInfo.innerHTML += ` ${key}: ${person[key]}<br>`;
                    }
                    wrapperForPersons.append(actorInfo);
                });
            });
        });
    });